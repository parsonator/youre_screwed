﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIControler : MonoBehaviour
{
     [HideInInspector]  public AIHealth health;
     [HideInInspector]  public AIMovement movement;
     [HideInInspector]  public AICombat combat;
     [HideInInspector]  public AIMasterControler masterController;

    float noiseOffset;

    private void Awake()
    {
        health = GetComponent<AIHealth>();
        movement = GetComponent<AIMovement>();
        combat = GetComponent<AICombat>();
        noiseOffset = Random.value * 10.0f;
    }

    public LayerMask searchLayer;

    // Update is called once per frame
    void Update()
    {
        if (masterController.playerTarget)
        {
            movement.MoveTowards(masterController.playerTarget.position, Time.deltaTime);
        }
    }

}