﻿using UnityEngine;
using UnityEngine.Events;

public class AIHealth : MonoBehaviour, IDamageable
{
	public int maxHealth = 3;
	public int health = 0;

	public UnityEvent onDamage;
	public UnityEvent onDeath;

	Animator m_Animator;
	
	public void Damage(int damage)
	{
		health -= damage;
		onDamage?.Invoke();

		if (IsDeath())
		{
			//if (m_Animator != null)
			//{
			//	m_Animator.Play("Death");
			//}

			//GetComponent<AIControler>().ReturnToPool();
			onDeath?.Invoke();
		}
	}

	public bool IsDeath()
	{
		return (health <= 0);
	}

	public void Init()
	{
		health = maxHealth;
	}

	private void Start()
	{
		Init();

		m_Animator = GetComponent<Animator>();
		if (m_Animator != null)
		{
			m_Animator.Play("Idle");
		}
	}

}
