﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIMasterControler : MonoBehaviour
{
	public static AIMasterControler Instance;

	public LayerMask searchLayer;

	public AIControler AIPrefab;

	public int spawnCount = 10;
	public float spawnRadius = 4.0f;
	public Transform playerTarget;

	private void Awake()
	{
		Instance = this;
	}

	public static AIControler Spawn(Vector2 position, AIControler aiPrefab)
	{
		//AIControler boid = aiPrefab.GetFromPool() as AIControler;
		//boid.transform.SetPositionAndRotation(position, Quaternion.identity);

		//boid.health.Init();
		
		var boid = Instantiate(aiPrefab, position, Quaternion.identity).GetComponent<AIControler>();

		boid.transform.localScale = Vector3.one * Random.Range(0.7f, 1.3f);

		boid.masterController = Instance;
		return boid;
	}
}