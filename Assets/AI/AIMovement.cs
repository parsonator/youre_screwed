﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIMovement : MonoBehaviour
{
    public float speed = 10;
    public Vector2 lookDirection;
    public Vector2 lastPos;
    public float stopDistance = 1;
    public Rigidbody2D Rigidbody2D;

    private void Awake()
    {
        Rigidbody2D = GetComponent<Rigidbody2D>();
    }

    public void MoveTowards(Vector2 target, float delta)
    {
        lastPos = Rigidbody2D.position;
        if (Vector2.Distance(target, Rigidbody2D.position) > stopDistance)
        {
            Rigidbody2D.position = Vector3.MoveTowards(transform.position, target, delta * speed);
        }
        lookDirection = (lastPos - Rigidbody2D.position).normalized;
    }
    

}