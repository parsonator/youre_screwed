﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageZone : MonoBehaviour
{
    public int damage = 1;

    private float tick = 0;


    private void OnTriggerEnter2D(Collider2D other)
    {
        tick = 0;
    }


    private void OnTriggerStay2D(Collider2D other)
    {
        tick += Time.fixedTime;
        if (tick > 1)
        {
            Debug.LogFormat("hit{0}", other.transform.name);
            var damageable = other.gameObject.GetComponentInParent<IDamageable>();
            damageable?.Damage(damage);
            tick = 0;
        }
    }
}