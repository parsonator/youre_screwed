﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ExtensionMethods
{
	public static Vector2 ToV2(this Vector3 vector3)
	{
		return new Vector2(vector3.x, vector3.y);
	}

	public static Vector3 ToV3(this Vector2 vector2)
	{
		return new Vector3(vector2.x, vector2.y);
	}

	public static Quaternion To2DRotation(this Vector2 lookAt, float angleOffset = 0f)
	{
		float rot_z = Mathf.Atan2(lookAt.y, lookAt.x) * Mathf.Rad2Deg;
		return Quaternion.Euler(0f, 0f, rot_z + angleOffset);
	}

	public static Vector2 To2DFacing(this Quaternion rotation, float angleOffset = 0f)
	{
		float angle = (rotation.eulerAngles.z + angleOffset) * Mathf.Deg2Rad;
		return new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));
	}

	public static Vector2 Rotate(this Vector2 v, float degrees)
	{
		float sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
		float cos = Mathf.Cos(degrees * Mathf.Deg2Rad);

		float tx = v.x;
		float ty = v.y;
		v.x = (cos * tx) - (sin * ty);
		v.y = (sin * tx) + (cos * ty);
		return v;
	}
}
