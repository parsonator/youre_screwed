﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnRect : MonoBehaviour
{
    [Range(0, 10)] public float width, height;

    public Vector2 GetPoint()
    {
        var point = transform.position.ToV2();
        point.x += Random.Range(-width / 2, width / 2);
        point.y += Random.Range(-height / 2, height / 2);
        return point;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawCube(transform.position, new Vector3(width, height));
    }
}