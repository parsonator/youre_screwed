﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Wave : IDisposable
{
    private List<AIControler> LiveAIControlers = new List<AIControler>();
    public WaveData WaveData;
    public Action OnWaveSpawnCompleat;
    public Action OnWaveStart;
    public List<GameObject> spawnPoints;
    private WaitForSeconds wait;


    public Wave(WaveData WaveData, List<GameObject> spawnPoints)
    {
        this.WaveData = WaveData;
        this.spawnPoints = spawnPoints;
    }

    public IEnumerator SpawnWave()
    {
        OnWaveStart?.Invoke();

		//WaveData.AIPool[0].EnsurePool(WaveData.AiCount);

        for (int i = 0; i < WaveData.AiCount; i++)
        {
            yield return wait;
            var spawn = spawnPoints[Random.Range(0, spawnPoints.Count)];
            SpawnRect SpawnRect = spawn.GetComponent<SpawnRect>();
            if (SpawnRect)
            {
                LiveAIControlers.Add(AIMasterControler.Spawn(SpawnRect.GetPoint(), WaveData.AIPool[0]));

            }
            else
            {
                LiveAIControlers.Add(AIMasterControler.Spawn(spawn.transform.position, WaveData.AIPool[0]));

            }
        }

        OnWaveSpawnCompleat?.Invoke();
    }

    public bool WaveComplete()
    {
        if (LiveAIControlers.Count == 0)
        {
            return false;
        }

        foreach (var aiControler in LiveAIControlers)
        {
            if (!aiControler.health.IsDeath())
            {
                return false;
            }
        }

        return true;
    }

    public void Dispose()
    {
        foreach (var liveAiControler in LiveAIControlers)
        {
            liveAiControler.ReturnToPool();
        }
    }
}