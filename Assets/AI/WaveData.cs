﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "WaveData", menuName = "Wave")]
public class WaveData : ScriptableObject
{
    public List<AIControler> AIPool;
    public int AiCount;
    public float SpawnDuration;
}