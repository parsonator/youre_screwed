﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditorInternal.Profiling.Memory.Experimental;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering;

public class WaveMaster : MonoBehaviour
{
    public static WaveMaster Instance;
    private Wave CurruntWave;
    public float PreWaveWait = 4;
    public static int TotalWaveNumber;
    public int localWaveNumber;
    public List<WaveData> waves;
    public List<GameObject> spawns;
    public UnityEvent OnWaveStart;
    public UnityEvent OnWaveCompleat;
    public UnityEvent OnAllWavesCompleat;

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        TotalWaveNumber = 0;
    }

    public void StartWave(List<WaveData> wavedata, List<GameObject> levelSpawns)
    {
        waves = wavedata;
        spawns = levelSpawns;
        localWaveNumber = 0;
        StartCoroutine(RunAllWaves());
    }

    IEnumerator RunAllWaves()
    {
        yield return WaveLoop();
        OnAllWavesCompleat?.Invoke();
    }

    IEnumerator WaveLoop()
    {
        yield return new WaitForSeconds(PreWaveWait);

        Debug.LogFormat("Spawning Wave {0}", TotalWaveNumber);
        CurruntWave = new Wave(waves[localWaveNumber], spawns);
        CurruntWave.OnWaveStart += () => Debug.Log("start Done");
        CurruntWave.OnWaveStart += () => OnWaveStart.Invoke();

        yield return CurruntWave.SpawnWave();
        yield return new WaitUntil(() => CurruntWave.WaveComplete());

        TotalWaveNumber++;
        localWaveNumber++;

        OnWaveCompleat.Invoke();

        if (waves.Count > localWaveNumber)
        {
            yield return WaveLoop();
        }
    }
}