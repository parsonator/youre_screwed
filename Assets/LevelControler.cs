﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;
using UnityEngine.Events;

public class LevelControler : MonoBehaviour
{
    public int CurruntSection;
    public List<LevelSection> levelsSections;
    [Space()] public UnityEvent OnLevelStart;
    public UnityEvent OnLevelCompleat;

    private WaveMaster WaveMaster;
    private PlayerInputController player;
    public CinemachineVirtualCamera dollyCam;

    private void Awake()
    {
        WaveMaster = GetComponent<WaveMaster>();
    }

    private bool startedSection = false;
    private bool levelCompleat = false;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindObjectOfType<PlayerInputController>();
        WaveMaster.OnAllWavesCompleat.AddListener(ProgressLevel);
    }

    // Update is called once per frame
    void Update()
    {
        if (levelCompleat) return;

        if (player)
        {
            if (levelsSections.Count <= CurruntSection)
            {
                levelCompleat = true;
                OnLevelCompleat.Invoke();
                Debug.Log("Level Compleat");
                return;
            } 
            if (player.transform.position.x >
                (levelsSections[CurruntSection].ZoneCamrea.transform.position.x -
                 levelsSections[CurruntSection].ZoneCamrea.m_Lens.OrthographicSize) && !startedSection)
            {
                WaveMaster.StartWave(levelsSections[CurruntSection].Waves, levelsSections[CurruntSection].Spawns);
                dollyCam.Priority = 0;
                 levelsSections[CurruntSection].ZoneCamrea.Priority = 20;

                startedSection = true;
            }
        }
    }

    public void ProgressLevel()
    {
        levelsSections[CurruntSection].ZoneCamrea.Priority = 0;
        dollyCam.Priority = 20;

        CurruntSection++;
        startedSection = false;
    }

    [System.Serializable]
    public class LevelSection
    {
        public List<GameObject> Spawns;
        public List<WaveData> Waves;
        public CinemachineVirtualCamera ZoneCamrea;
        public float startPointX;
    }

    private void OnDrawGizmos()
    {
        for (var index = 0; index < levelsSections.Count; index++)
        {
            var levelsWave = levelsSections[index];
            Gizmos.color = new Color(0, 1 - (index * 0.2f), 0, 1f);
            var point = Vector3.right * levelsWave.startPointX;
            Gizmos.DrawLine((point) + Vector3.down * 10, point + Vector3.up * 10);
        }
    }
}