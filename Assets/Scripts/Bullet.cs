﻿using UnityEngine;

public class Bullet : MonoBehaviour
{
	int m_Damage;
	float m_MaxLifetime;

	public LayerMask HitLayers;
	
	public Transform Trans { get; private set; }
	Vector3 m_Position;
	Vector3 m_MoveDir;
	float m_Speed;
	
	[SerializeField] private RaycastHit2D Hit;

	SpriteRenderer m_SpriteRenderer;

	void Awake()
	{
		Trans = transform;
		m_SpriteRenderer = GetComponent<SpriteRenderer>();
	}

	public void Init(WeaponParams weaponParams, Vector2 position, Vector2 spawnPosOffset, Vector2 direction, float angleOffset = 0f)
	{
		m_Damage = (int)weaponParams.m_Damage;
		m_MaxLifetime = weaponParams.m_Range / weaponParams.m_Speed;
		

		m_SpriteRenderer.color = Random.ColorHSV(0, 1, 0.4f, 0.7f, 0.9f, 1);

		Vector2 rotatedOffset = spawnPosOffset.Rotate(Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg);
		m_Position = position + rotatedOffset;
		
		Vector2 fireDir = direction;
		if (angleOffset != 0f)
		{
			fireDir = direction.Rotate(angleOffset);
		}
		
		m_MoveDir = fireDir.ToV3();
		m_Speed = weaponParams.m_Speed;
		
		Trans.SetPositionAndRotation(m_Position, fireDir.To2DRotation());
	}

	void Update()
	{
		float deltaTime = Time.deltaTime;
		m_MaxLifetime -= deltaTime;

		if (m_MaxLifetime < 0)
		{
			this.ReturnToPool();
			return;
		}
		
		float moveDistThisFrame = m_Speed * deltaTime;

		Hit = Physics2D.Raycast(Trans.position, m_MoveDir, moveDistThisFrame, HitLayers);
		if (Hit.collider != null)
		{
			Hit.collider.GetComponentInParent<IDamageable>()?.Damage(m_Damage);
			this.ReturnToPool();
		}

		m_Position += m_MoveDir * moveDistThisFrame;
		Trans.position = m_Position;
	}
}
