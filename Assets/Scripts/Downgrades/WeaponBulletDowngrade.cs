﻿using System;
using UnityEngine;

[CreateAssetMenu]
public class WeaponBulletDowngrade : WeaponDowngrade
{
	public WeaponParams.BulletSpawnParams[] m_ReplacementBullets;
}
