﻿using System;
using UnityEngine;

[CreateAssetMenu]
public class WeaponStatDowngrade : WeaponDowngrade
{
	public float m_ValueModifier;
}
