﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteController))]
public class PlayerInputController : MonoBehaviour
{
	public float m_MoveSpeed = 5;
	
	public WeaponComponent m_WeaponComponent;
	public Transform m_BulletOriginR;
	public Transform m_BulletOriginL;
	public Transform m_BulletOriginUp;
	public Transform m_BulletOriginDown;
	
	public int shotCountDegrader = 10;
	int currentShotCount = 0;

	SpriteController m_SpriteController;
	Transform m_BulletOrigin;
	
	const float kStickDeadZone = 0.3f;

	void Awake()
	{
		m_SpriteController = GetComponent<SpriteController>();

		m_WeaponComponent.Reset();
	}
	
	void Update()
	{
		HandleMovement();
		HandleFiring();
	}

	void HandleMovement()
	{
		Vector2 LStick = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
		Vector2 RStick = new Vector2(Input.GetAxis("RHor"), Input.GetAxis("RVert"));
		
		Vector2 walkDir = LStick;
		if (walkDir.magnitude > 1f)
		{
			walkDir = walkDir.normalized;
		}
		transform.position += walkDir.ToV3() * m_MoveSpeed * Time.deltaTime;
		

		bool updateAim = false;
		Vector2 aimDir = Vector2.zero;
		if (RStick.magnitude > kStickDeadZone)
		{
			updateAim = true;
			aimDir = RStick.normalized;
		}
		else if (LStick.magnitude > kStickDeadZone)
		{
			updateAim = true;
			aimDir = LStick.normalized;
		}

		if (updateAim)
		{
			Vector2 facingDir;
			// Restrict rotation to cardinal directions
			if (Mathf.Abs(aimDir.x) >  Mathf.Abs(aimDir.y))
			{
				bool isLeft = aimDir.x < 0;
				facingDir = isLeft ? Vector2.left : Vector2.right;

				m_BulletOrigin = isLeft ? m_BulletOriginL : m_BulletOriginR;
			}
			else
			{
				bool isUp = aimDir.y > 0;
				facingDir = isUp ? Vector2.up: Vector2.down;
				
				m_BulletOrigin = isUp ? m_BulletOriginUp : m_BulletOriginDown;
			}
			
			m_SpriteController.SetMovementAndFacing(walkDir, facingDir);
		}
	}

	void HandleFiring()
	{
		bool isFiring = Input.GetAxis("RTrigger") > kStickDeadZone;
	
		if (isFiring)
		{
			bool fired = m_WeaponComponent.TryFire(m_BulletOrigin, m_BulletOrigin.rotation.To2DFacing());

			if (fired)
			{
				++currentShotCount;

				if (currentShotCount % shotCountDegrader == 0)
				{
					m_WeaponComponent.DowngradeWeapon();
				}
			}
		}
	}
}
