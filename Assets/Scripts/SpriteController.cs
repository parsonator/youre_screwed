﻿using UnityEngine;

//[RequireComponent(typeof(Animator))]
public class SpriteController : MonoBehaviour
{
	Animator m_Animator;
	SpriteRenderer m_SpriteRenderer;
	Vector2 m_LastDir = Vector2.zero;
	string m_CurrentAnim;

	// Matches state machine names - Don't change!
	enum WalkAnims
	{
		Left,
		Right,
		Down,
		Up,
		Unchanged
	}

	public void SetMovementAndFacing(Vector2 moveVector, Vector2 direction)
	{
		WalkAnims walkAnim = WalkAnims.Unchanged;

		if (direction != m_LastDir)
		{
			if (direction.y == 0)
			{
				walkAnim = WalkAnims.Right;

				// Horizontal & Sprite flip
				m_SpriteRenderer.flipX = direction.x < 0;
			}
			else
			{
				walkAnim = direction.y > 0 ? WalkAnims.Up : WalkAnims.Down;
				m_SpriteRenderer.flipX = false;
			}

			m_LastDir = direction;
		}

		
		if (walkAnim != WalkAnims.Unchanged)
		{
			string stateName = walkAnim.ToString();
			m_Animator.Play(stateName);
		}
		bool isMoving = moveVector.magnitude > 0.1f;
		m_Animator.speed = isMoving ? 1 : 0;

		//transform.rotation = direction.To2DRotation();
	}
	
	void Awake()
	{
		//m_Animation = GetComponent<Animation>();
		m_Animator = GetComponent<Animator>();
		m_SpriteRenderer = GetComponent<SpriteRenderer>();

		//m_Animation.Play(kAnimNames[0]);
	}
}
