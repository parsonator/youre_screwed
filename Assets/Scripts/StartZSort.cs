﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartZSort : MonoBehaviour
{
    private SpriteRenderer _spriteRenderer;

    // Start is called before the first frame update
    void Start()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _spriteRenderer.sortingOrder = Mathf.RoundToInt(transform.position.y * 70f) * -1;
    }
}