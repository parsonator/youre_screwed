﻿using System;
using UnityEngine;

[CreateAssetMenu]
public class WeaponComponent : ScriptableObject
{
	public WeaponParamsAsset m_CurrentWeapon;
	
	WeaponParams m_CurrentWeaponParams;
	
	float m_LastFireTime;

	int m_NumActiveDowngrades;

	public void Reset()
	{
		m_LastFireTime = 0f;
		m_NumActiveDowngrades = 0;
		
		m_CurrentWeaponParams = m_CurrentWeapon.m_WeaponParams;
	}

	public bool TryFire(Transform parentTrans, Vector2 direction)
	{
		bool fired = false;

		float timeNow = Time.time;
		if (timeNow > m_LastFireTime + m_CurrentWeaponParams.m_FireDelay)
		{
			Vector2 basePos = parentTrans.position.ToV2();
			Vector2 playerFacing = parentTrans.rotation.To2DFacing();
			
			float shotOffset = (0.5f - UnityEngine.Random.value * m_CurrentWeaponParams.m_FireConeArc);
			Vector2 fireDir = playerFacing.Rotate(shotOffset);

			for (int i = 0; i < m_CurrentWeaponParams.m_Bullets.Length; ++i)
			{
				m_CurrentWeaponParams.m_Bullets[i].Spawn(m_CurrentWeaponParams, basePos, fireDir);
			}
			
			m_LastFireTime = timeNow;
			fired = true;
		}

		return fired;
	}

	public void DowngradeWeapon()
	{
		m_CurrentWeaponParams = m_CurrentWeapon.m_WeaponParams;

		// Increment
		m_NumActiveDowngrades = Mathf.Min(m_NumActiveDowngrades + 1, m_CurrentWeaponParams.m_AvailableDowngrades.Length - 1);
		
		for (int i = 0; i < m_NumActiveDowngrades; ++i)
		{
			m_CurrentWeaponParams.ApplyDowngrade(m_CurrentWeapon.m_WeaponParams.m_AvailableDowngrades[i]);
		}
	}
}
