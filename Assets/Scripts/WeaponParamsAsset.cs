﻿using System;
using UnityEngine;

[CreateAssetMenu]
public class WeaponParamsAsset : ScriptableObject
{
	public WeaponParams m_WeaponParams;
}

[Serializable]
public struct WeaponParams
{
	public enum DowngradeType
	{
		ReplaceBullet,
		StatFireDelay,
		StatDamage,
		StatRange,
		StatSpeed,
		StatAccuracy,// cone
		StatAimDirection, // centre line offset
		//overheat?
	}
	
	[Serializable]
	public struct BulletSpawnParams
	{
		public Bullet m_BulletPrefab;
		public Vector2 m_SpawnPosOffset;

		public float m_SpawnAngleOffset;

		public void Spawn(WeaponParams weaponParams, Vector2 position, Vector2 direction)
		{
			Bullet bulletInstance = m_BulletPrefab.GetFromPool() as Bullet;
			
			bulletInstance.Init(weaponParams, position, m_SpawnPosOffset, direction, m_SpawnAngleOffset);
		}
	}

	public BulletSpawnParams[] m_Bullets;
	
	public float m_FireDelay;//s delay between shots
	public float m_Speed;
	public float m_Damage;
	public float m_Range;
	public float m_FireConeArc;// Size of whole arc
	
	public WeaponDowngrade[] m_AvailableDowngrades;

	public void ApplyDowngrade(WeaponDowngrade downgrade)
	{
		switch (downgrade.m_DowngradeType)
		{
			case DowngradeType.ReplaceBullet:
				m_Bullets = (downgrade as WeaponBulletDowngrade).m_ReplacementBullets;
				break;
			case DowngradeType.StatFireDelay:
				m_FireDelay += (downgrade as WeaponStatDowngrade).m_ValueModifier;
				break;
			case DowngradeType.StatDamage:
				m_Damage += (downgrade as WeaponStatDowngrade).m_ValueModifier;
				break;
			case DowngradeType.StatRange:
				m_Range += (downgrade as WeaponStatDowngrade).m_ValueModifier;
				break;
			case DowngradeType.StatSpeed:
				m_Speed += (downgrade as WeaponStatDowngrade).m_ValueModifier;
				break;
			case DowngradeType.StatAccuracy:
				m_FireConeArc += (downgrade as WeaponStatDowngrade).m_ValueModifier;
				break;
			case DowngradeType.StatAimDirection:
				//m_FireDelay += (downgrade as WeaponStatDowngrade).m_ValueModifier;
				break;
			default:
				break;
		}
	}
}
