﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class UIWaveCount : MonoBehaviour
{
    private Text UIText;

    // Start is called before the first frame update
    void Start()
    {
        UIText = GetComponent<Text>();
        WaveMaster.Instance.OnWaveCompleat.AddListener(UpdateCounter);
    }

    // Update is called once per frame
    void UpdateCounter()
    {
        UIText.text = "" + WaveMaster.TotalWaveNumber;
    }
}